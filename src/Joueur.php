<?php

namespace App;

abstract class Joueur
{
    private $cash;

    public function encaisser($somme)
    {
        $this->cash = $this->cash + $somme;
    }

    public function payer($somme)
    {
        $this->cash = $this->cash - $somme;
    }

    public function acheter($prixBien)
    {
        $this->cash = $this->cash - $prixBien;
    }

    public function vendre($prixBien)
    {
        $this->cash = $this->cash + $prixBien;
    }
}
