<?php

namespace App;

class Banque extends Joueur
{
    /**
     * Instance unique du singleton
     * @var Singleton
     * @access private
     * @static
     */
    private static $_instance = null;

    private function __construct()
    {

    }

    public static function getInstance()
    {

        if (is_null(self::$_instance)) {
            self::$_instance = new Banque();
        }

        return self::$_instance;

    }
}